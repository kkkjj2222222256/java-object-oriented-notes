# 笔记

```java
 扫描器
     例子：
Scanner sc1 = new Scanner(System.in);
System.out.println("输入一个数字:");
int a = sc1.nextInt();//根据类型变换
基本数据类型：
    整数：byte，short，int，long
    浮点数：float，double
    字符：char
    布尔：boolean
如果语句：
    if(条件){
        满足条件的结果
    }else{
        为满足条件的结果
    }
```



# 作业

```java
package abab;

import java.util.Scanner;

public class av {
    public static void main(String[] args) {
        //**1、判断一个字符数据是否是数字字符 **
        //
        //**分析：**
        //
        //​	1、需要判断一个字符是否是数字字符，首先需要提供一个字符数据
        //
        //​	2、字符是否为数字字符： 数字字符的范围 0 - 9 之间都属于数字字符，因此提供的字符只要大于或等于字符0，并且还要下于或等于字符9即可。
        //
        //​	3、判断完成之后，打印判断的结果。
        //
        Scanner sc1 = new Scanner(System.in);
        System.out.println("输入一个数字:");
        int a = sc1.nextInt();
        if (a>=0 && a<=9){
            System.out.println(a+"属于数字字符");
        }else{
            System.out.println(a+"不属于数字字符");
        }
        //
        //
        //**2、判断一个字符数据是否是字母字符**
        //
        //**分析：**
        //
        //​	1、需要判断一个字符是否是字母字符，首先需要提供一个字符数据
        //
        //​	2、字符是否为字母字符： 数字字符的范围 a - z 或者 A - Z 之间都属于字母字符，因此提供的字符只要大于或等于a，并且还要下于或等于z 或者 大于或等于A，并且还要下于或等于Z
        //
        //​	3、判断完成之后，打印判断的结果。
        //
        Scanner sc2 = new Scanner(System.in);
        System.out.println("输入一个字母:");
        char b = sc2.nextLine().charAt(0);
        if(b>='a' && b<='z' || b>='A' && b<='Z'){
            System.out.println(b+"是子母字符");
        }else{
            System.out.println(b+"不是子母字符");
        }
        //
        //
        //**3、判断指定的年份是否为闰年，请使用键盘录入**
        //
        //**分析：**
        //
        //​	1、闰年的判断公式为：能被4整除，但是不能被100整除 或者 能被400整除
        //
        //​	2、首先需要提供一个需要判断的年份，判断完成之后，打印判断的结果。
        //
        Scanner sc3 = new Scanner(System.in);
        System.out.println("输入一个年份:");
        int x = sc3.nextInt();
        if(x%4==0 && x%100!=0 || x%400==0){
            System.out.println(x+"是闰年");
        }else{
            System.out.println(x+"不是闰年");
        }
        //
        //
        //**4、判断一个数字是否为水仙花数,请使用键盘录入**
        //
        //水仙花是指3位数字，表示的是每位上的数字的3次幂相加之后的和值和原数相等，则为水仙花数，
        //
        //**分析：**
        //
        //​	如：153  --->  1×1×*1 + 5*×5×*5 + 3×*3×3 = 153; 就是水仙花数
        //
        //​		1、首先需要提供一个需要判断的3位数字，因此需要一个数值
        //
        //​		2、判断的过程
        //
        //​			a) 将3位数字的每一位上的数字拆分下来
        //
        //​			b) 计算每位数字的3次幂之和
        //
        //​			C) 用和值 和 原来的数字进行比较
        //
        //​		D) 打印判断的比较结果即可
        //
        Scanner sc4 = new Scanner(System.in);
        System.out.println("输入一个三位数:");
        int y = sc4.nextInt();
        int y1=y%10;
        int y2=y/10%10;
        int y3=y/100%10;
        int he=y1*y1*y1+y2*y2*y2+y3*y3*y3;
        if (y==he){
            System.out.println(y+"是水仙花数");
        }else{
            System.out.println(y+"不是水仙花数");
        }
        //
        //
        //**5、判断一个5位数字是否为回文数，使用键盘录入**
        //
        //五位数的回文数是指最高位和最低位相等，次高位和次低位相等。如：12321  23732  56665
        //
        //**分析：**
        //
        //​	1、首先需要提供一个需要判断的5位数字，因此需要一个数值
        //
        //​	2、判断的过程
        //
        //​		a) 将5位数字的万、千、十、个位数拆分出来
        //
        //​		b) 判断比较万位和个位 、 千位和十位是否相等
        //
        //​	3、判断完成之后，打印判断的结果。
        Scanner sc5 = new Scanner(System.in);
        System.out.println("输入一个五位数:");
        int s = sc5.nextInt();
        int s1=s%10;
        int s2=s/10%10;
        int s3=s/100%10;
        int s4=s/1000%10;
        int s5=s/10000%10;
        if(s1==s5 && s2==s4){
            System.out.println(s+"是回文数");
        }else{
            System.out.println(s+"不是回文数");
        }
    }
}
```

